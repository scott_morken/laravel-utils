<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/15/14
 * Time: 7:25 AM
 */

use Smorken\Utils\StrUtils;

class StrUtilsTest extends PHPUnit_Framework_TestCase {

    public function testToStringWithString()
    {
        $test = 'foo';
        $this->assertStringStartsWith($test, StrUtils::toString($test));
    }

    public function testToStringWithSingleDArray()
    {
        $test = array('foo');
        $this->assertStringStartsWith('0: foo', StrUtils::toString($test));
    }

    public function testToStringWithSingleDArrayKeyed()
    {
        $test = array('foo' => 'bar');
        $this->assertStringStartsWith('foo: bar', StrUtils::toString($test));
    }

    public function testToStringWithSingleDArrayMultiple()
    {
        $test = array('foo', 'bar');
        $expected = "0: foo\n1: bar\n";
        $this->assertEquals($expected, StrUtils::toString($test));
    }

    public function testToStringWithSingleDArrayKeyedMultiple()
    {
        $test = array('foo' => 'bar', 'baz' => 'bat');
        $expected = "foo: bar\nbaz: bat\n";
        $this->assertEquals($expected, StrUtils::toString($test));
    }

    public function testToStringWithMultiDArray()
    {
        $test = array('foo' => array('bar'));
        $expected = "foo => \n 0: bar\n";
        $this->assertEquals($expected, StrUtils::toString($test));
    }

    public function testToStringWithMultiDArrayWithMulti()
    {
        $test = array('foo' => array('bar', 'baz'));
        $expected = "foo => \n 0: bar\n 1: baz\n";
        $this->assertEquals($expected, StrUtils::toString($test));
    }

    public function testToStringwithMultiDArrayMultipleWithMulti()
    {
        $test = array(
            'foo' => array('bar', 'baz'),
            'fiz' => array('boz', 'biz'),
        );
        $expected = "foo => \n 0: bar\n 1: baz\nfiz => \n 0: boz\n 1: biz\n";
        $this->assertEquals($expected, StrUtils::toString($test));
    }

    public function testToStringWithMultiDArrayWithMultiD()
    {
        $test = array(
            'foo' => array(
                'bar' => array('bit'),
                'baz' => array('bat')
            ),
        );
        $expected = "foo => \n bar => \n  0: bit\n baz => \n  0: bat\n";
        $this->assertEquals($expected, StrUtils::toString($test));
    }
} 