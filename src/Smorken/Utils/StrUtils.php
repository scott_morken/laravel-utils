<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/15/14
 * Time: 7:15 AM
 */

namespace Smorken\Utils;


class StrUtils
{

    public static function toString($value, $depth = 0, $repeat = ' ', $eol = PHP_EOL)
    {
        $str = '';
        $rep = str_repeat($repeat, $depth);
        if (!is_array($value)) {
            $str .= $value . $eol;
        }
        else {
            foreach($value as $k => $v) {
                $str .= "$rep$k";
                $d = $depth;
                if (is_array($v)) {
                    $str .= ' => ' . $eol;
                    $d = $depth + 1;
                }
                else {
                    $str .= ': ';
                }
                $str .= self::toString($v, $d, $repeat, $eol);
            }
        }
        return $str;
    }

}