<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 5/15/14
 * Time: 7:16 AM
 */

namespace Smorken\Utils;


class DateTimeUtils {

    public static function ago($time, $append = false)
    {
        $periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
        $lengths = array("60","60","24","7","4.35","12","10");
        $appendformat = array(null, "g:i a", "g:i a", "g:i a", "M d", "M d", "M d, Y");

        $now = time();

        $difference     = $now - $time;
        $tense         = "ago";

        for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
            $difference /= $lengths[$j];
        }

        $difference = round($difference);

        if($difference != 1) {
            $periods[$j].= "s";
        }

        $str = "$difference $periods[$j] ago";

        if ($append) {
            $str  = date($appendformat[$j], $time) . " ($str)";
        }

        return $str;
    }

} 