<?php
/**
 * Created by PhpStorm.
 * User: smorken
 * Date: 3/21/14
 * Time: 9:41 AM
 */

namespace Smorken\Utils;


class Utils {

    public static function containsArray($array)
    {
        if (!is_array($array) || (is_array($array) && empty($array))) {
            return false;
        }
        foreach($array as $v) {
            if (is_array($v)) {
                return true;
            }
        }
        return false;
    }

    public static function containsAssocArray($array)
    {
        if (!is_array($array) || (is_array($array) && empty($array))) {
            return false;
        }
        foreach($array as $k => $v) {
            if (is_array($v)) {
                $check = array_keys($v)[0];
                if (!is_integer($check)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function containsAllArrays($array)
    {
        if (!is_array($array) || (is_array($array) && empty($array))) {
            return false;
        }
        foreach($array as $v) {
            if (!is_array($v)) {
                return false;
            }
        }
        return true;
    }

}